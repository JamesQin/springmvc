package com.james.controller;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.james.bean.User;

@Controller
public class UserController {
	@RequestMapping("register.do")
	public String register(@RequestParam(required=true,value="name")String userName,
			@RequestParam(required=false,value="birthday")long userBirthday,
			@RequestParam(required=false,value="gender",defaultValue="0")int userGender,Model model){
		User u = new User();
		u.setUserId(new Random().nextLong());
		u.setUserName(userName);
		u.setUesrBirthday(userBirthday);
		u.setUserGender(userGender);
		model.addAttribute("id",u.getUserId());
		model.addAttribute("name",u.getUserName());
		return "Home";
	}
}
