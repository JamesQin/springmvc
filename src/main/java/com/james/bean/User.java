package com.james.bean;

public class User {
	private long userId;
	private String userName;
	private long uesrBirthday;
	private int userGender;
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getUesrBirthday() {
		return uesrBirthday;
	}
	public void setUesrBirthday(long uesrBirthday) {
		this.uesrBirthday = uesrBirthday;
	}
	public int getUserGender() {
		return userGender;
	}
	public void setUserGender(int userGender) {
		this.userGender = userGender;
	}
}
